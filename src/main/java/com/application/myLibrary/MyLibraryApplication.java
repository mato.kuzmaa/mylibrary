package com.application.myLibrary;

import com.application.myLibrary.entity.Author;
import com.application.myLibrary.entity.Book;
import com.application.myLibrary.entity.Category;
import com.application.myLibrary.entity.Publisher;
import com.application.myLibrary.repository.PublisherRepository;
import com.application.myLibrary.service.BookService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MyLibraryApplication {
	private final PublisherRepository publisherRepository;

	public MyLibraryApplication(PublisherRepository publisherRepository) {
		this.publisherRepository = publisherRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(MyLibraryApplication.class, args);
	}

	@Bean
	public CommandLineRunner initialCreate(BookService bookService) {
		return (args) -> {
			Book book1 = new Book("ABC", "Janko Hrasko", "Very interesting book");
			Author author1 = new Author("Ludmila Podjavorinska", "Slovak author");
			Category category1 = new Category("Fairy tales");
			Publisher publisher1 = new Publisher("Tatran");
			book1.addAuthor(author1);
			book1.addCategory(category1);
			book1.addPublisher(publisher1);
			bookService.createBook(book1);

			Book book2 = new Book("DEF", "Success Man", "Boring book");
			Author author2 = new Author("Jan Dlhy", "Czech author");
			Category category2 = new Category("Business books");
			Publisher publisher2 = new Publisher("Slovart");
			book2.addAuthor(author2);
			book2.addCategory(category2);
			book2.addPublisher(publisher2);
			bookService.createBook(book2);

			Book book3 = new Book("GHI", "Snehulienka", "Exciting book");
			Author author3 = new Author("Jacob Grimm", "Polish author");
			Category category3 = new Category("Science books");
			Publisher publisher3 = new Publisher("Rak");
			book3.addAuthor(author3);
			book3.addCategory(category3);
			book3.addPublisher(publisher3);
			bookService.createBook(book3);
		};
	}
}
