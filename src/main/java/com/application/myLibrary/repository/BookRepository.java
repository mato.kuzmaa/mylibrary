package com.application.myLibrary.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.application.myLibrary.entity.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
}
